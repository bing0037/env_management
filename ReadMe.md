# Func: this repo is used to store and recover anaconda environments.

# How to save current environment:
```
conda-env  export -n pytorch_test > pytorch_test.yml
conda-env  export -n tf-gpu-cuda10 > tf-gpu-cuda10.yml
```

# How to load previous environment:
```
# Delete current environment first!
conda-env create -n pytorch_test -f=pytorch_test.yml
conda-env create -n tf-gpu-cuda10 -f=tf-gpu-cuda10.yml
```
