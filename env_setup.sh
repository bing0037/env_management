# Manual
#cd $HOME
#git clone https://bing0037@bitbucket.org/bing0037/env_management.git


# Auto
cd /tmp 
curl -O https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
bash Anaconda3-2019.03-Linux-x86_64.sh
source ~/.bashrc

cd $HOME/env_management
conda-env create -n pytorch -f=pytorch_test.yml
conda-env create -n tensorflow -f=tf-gpu-cuda10.yml
cd ..
rm -rf env_management
