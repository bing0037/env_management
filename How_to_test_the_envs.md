# 1) Test the tensorflow env using HW1:
cd $HOME
midir code
cd code
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1L99C4Mcx8FqUB8wh8lCXf7zy4cqANwdM' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1L99C4Mcx8FqUB8wh8lCXf7zy4cqANwdM" -O HW1.zip && rm -rf /tmp/cookies.txt

unzip HW1.zip
cd HW1/sample_code1
conda activate tensorflow

# If the first GPU is occupied, you choose change to another one (CUDA_VISIBLE_DEVICES=0/1/2/3):
CUDA_VISIBLE_DEVICES=3 python classify.py test test.jpg


# 2) Test pytorch using pytorch_tutorial:
cd $HOME/code
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1vF0LSseOaR_2jqCjDtheWRaXw96Ftk_I' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1vF0LSseOaR_2jqCjDtheWRaXw96Ftk_I" -O pytorch_tutorial.zip && rm -rf /tmp/cookies.txt

unzip pytorch_tutorial.zip
cd pytorch_tutorial
conda activate pytorch
python 3_train_on_GPU.py

# 3) Test NLP:
cd $HOME/code/NLP/fairseq

wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1TwxuMRgzjRVdtV1TXt5fMl0MdZF4Xcfh' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1TwxuMRgzjRVdtV1TXt5fMl0MdZF4Xcfh" -O How_to_use.zip && rm -rf /tmp/cookies.txt

unzip How_to_use.zip
cd How_to_use



